import { Module } from '@nestjs/common'
import { AppController } from './App.controller'
import { AppService } from './app.service'

import { CatsModule } from '../modules/Cats/Cats.module'

@Module({
  imports: [CatsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

import { Get, Controller } from '@nestjs/common'
import { CatsService } from './Cats.service'

@Controller('cats')
export class CatsController {

  constructor(private readonly catsService: CatsService) {}

  @Get()
  getHello(): string {
    return this.catsService.getCatsGreeting()
  }
}

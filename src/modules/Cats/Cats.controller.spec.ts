import { Test, TestingModule } from '@nestjs/testing'
import { CatsController } from './Cats.controller'
import { CatsService } from './Cats.service'

describe('CatsController', () => {
  let cats: TestingModule

  beforeAll(async () => {
    cats = await Test.createTestingModule({
      controllers: [CatsController],
      providers: [CatsService],
    }).compile()
  })

  describe('getHello', () => {
    it('should return "Hello World!"', () => {
      const catsController = cats.get<CatsController>(CatsController)
      expect(catsController.getHello()).toBe('Cats Hello!')
    })
  })
})

import { Injectable } from '@nestjs/common'

@Injectable()
export class CatsService {
  getCatsGreeting(): string {
    return 'Cats Hello!'
  }
}
